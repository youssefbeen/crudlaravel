
## How to run


* Backend Laravel Api:

1. Run `git clone https://youssefbeen@bitbucket.org/youssefbeen/crudlaravel.git`

2. Run `composer install` (in the project folder)

3. create database for project (MySql)

4. copy (.env.example to .env) and enter database info (database, username, password)

5. Run `php artisan migrate`

6. Run `php artisan db:seed`

7. Run `php artisan passport:install` (After executing the previous command, you should copy the client_secret of 
client_id = '2' and save it)

9. Run `php artisan key:generate`

10. Run `php artisan serve` or run the application on your own server

* Frontend VueJs:

1. Run `git clone https://youssefbeen@bitbucket.org/youssefbeen/crudvuejs.git`

2. Run `npm install` (in the vuejs project folder)

3. Edit "config/dev.env.js";
  * put your base url (default http://localhost:8000), and your client_secret (from step 7 on 
backend configuration)

4. Run `npm run dev`

###Login

* E-mail: admin@admin.com
* password: admin


