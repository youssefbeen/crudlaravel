<?php

use Illuminate\Database\Seeder;

class MembersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Member::class, 30)->create([
            "first_name" => 'Youssef',
            "last_name" => "Bentaleb",
            "image" => "ZY7KhWpjEPjn2w3G.png"
        ]);
    }
}
