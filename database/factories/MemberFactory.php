<?php

use Faker\Generator as Faker;

$factory->define(\App\Member::class, function (Faker $faker) {
    return [
        'first_name' => $faker->text(10),
        'last_name' => $faker->text(10),
        'image' => $faker->text(5),
    ];
});
