<?php

namespace App\Http\Controllers;

use App\Http\Resources\Member as MemberResource;
use App\Member;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class MemberController extends Controller
{

    public function store_image($image)
    {

        $exploded = explode(',', $image);
        $decoded = base64_decode($exploded[1]);

        if (str_contains($exploded[0], 'jpeg'))
            $extension = 'jpg';
        else
            $extension = 'png';

        $fileName = str_random() . '.' . $extension;
        $path = public_path() . '/api/img/' . $fileName;
        file_put_contents($path, $decoded);

        return $fileName;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if ($request->input('limit')) {
            $limit = $request->input('limit');
        } else {
            $limit = Member::all()->count();
        }

        $name = $request->input('title');
        if ($name) {
            //filter by fullname
            $names = explode(" ", $name);
            if (sizeof($names) == 1) $names[1] = "%";
            $members = Member::where('first_name', 'LIKE', '%' . $names[0] . '%')->where('last_name', 'LIKE', '%'
                . $names[1] . '%')->orWhere('last_name', 'LIKE', '%'
                . $names[0] . '%')->where('first_name', 'LIKE', '%' . $names[1] . '%');

        } else {
            $members = DB::table('members');
        }

        $members = $members->paginate($limit);

        return MemberResource::collection($members);


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $member = Member::findOrFail($id);
        return new MemberResource($member);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validatedData = $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
//            'image' => 'required',
        ]);
        $member = new Member();
        $image = $request->input('image');
        ($image) ? $member->image = $this->store_image($image) : null;

        $member->first_name = $request->input("first_name");
        $member->last_name = $request->input("last_name");
        if ($member->save()) {
            return new MemberResource($member);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Log::info($request);
        $member = Member::findOrFail($id);
        $image = $request->input('image');
        ($image) ? $member->image = $this->store_image($image) : null;
        $member->first_name = $request->input("first_name");
        $member->last_name = $request->input("last_name");
        if ($member->update()) {
            return new MemberResource($member);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $member = Member::findOrFail($id);

        if ($member->delete()) {
            return new MemberResource($member);
        }

    }
}
